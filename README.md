# Ideation Notebook

## Kyle Kane

### Issue Statement:
    The increase in multiple streaming subscriptions is becoming normal in today's society. 
    If there was a web application where you could search your movie or tv show 
    then users could more easily find their desired entertainment without 
    searching through every movie and show which would be better 
    because users would have less headache searching and more fun watching.

### Justification
    Netflix, Hulu, and Amazon prime have over 50,000 titles. 
    Most users have multiple subscriptions to those three and many more. 
    The case is simple, these three streaming companies pride themselves on unique content 
    which starts to become an inconvinence with multiple subscriptions. 

    It would be helpful as a user to be able to search one application 
    that lists where that application can be streamed from or rent/bought. 
    The only solution right now is to seach through over 50,000 titles looking for one matching case. 
    Those numbers just aren't efficient. 

    This application would also include links to the streaming service/website that has the matching title.
    This application would also include reviews from multiple sites e.g. Metacritic, Rotten Tomatoes.